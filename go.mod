module gitlab.com/psns/sie

go 1.15

require (
	github.com/alexflint/go-arg v1.3.0
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/copier v0.2.3
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/stretchr/testify v1.6.1
	github.com/toqueteos/webbrowser v1.2.0
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
