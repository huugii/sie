import menuBar from './MenuBar/MenuBar'
import infoDisplay from './InfoDisplay/InfoDisplay'
import canvas from './Canvas/Canvas'
import elementList from './ElementList/ElementList'

export const MenuBar = menuBar
export const ElementList = elementList
export const Canvas = canvas
export const InfoDisplay = infoDisplay
