import websocket from './websocket'

export const API = websocket.API
export const SendRequest = websocket.sendRequest
export const CloseRequest = websocket.closeRequest