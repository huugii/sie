import P5 from 'p5'
import conf from '../conf/grid'
import {Drawer, AppContext} from '@/utils'

class Grid {
    // General variables
    instance = null
    simFrame = 0
    simBaseParticles = []

    elementID = ''
    parentID = ''
    cnv
    p
    shapesList = []
    particlesList = []
    canvasElement
    selectedElement = -1

    cx = 0
    cy = 0
    rx = .5
    ry = .5
    drawer = new Drawer()

    constructor(elementID, parentID) {
        this.themeID = 'dark'
        this.theme = conf[this.themeID]
        this.elementID = elementID
        this.parentID = parentID
        this.instance = new P5(p => {
            this.p = p
            this.drawer.SetP(p)
            p.setup = () => this.setup()
            p.draw = () => this.draw()
            p.mouseDragged = e => this.onMouseDragged(e)
            p.windowResized = () => this.onWindowResized()
            p.mouseReleased = () => this.onMouseReleased()
            p.mousePressed = () => this.onMousePressed()
        })
    }

    CenterOnParticleId(id) {
        const particle = this.particlesList[id]
        const screenCenterX = document.getElementById(this.parentID).offsetWidth / 2
        const screenCenterY = document.getElementById(this.parentID).offsetHeight / 2
        this.setAbsCenter(screenCenterX - particle.p.x, screenCenterY + particle.p.y)
    }

    /**
     * Called on init of P5
     */
    setup() {
        this.cnv = this.p.createCanvas(document.getElementById(this.parentID).offsetWidth, document.getElementById(this.parentID).offsetHeight)
        this.cnv.parent(this.elementID)
        this.setRelCenter(this.rx, this.ry)
    }

    /**
     * Called each frame (60/s)
     */
    draw() {
        this.drawer.SetOffset(this.cx, this.cy)
        this.p.background(this.theme.background)
        this.drawMultipleAxis(20, 55, 0.7)
        this.drawMultipleAxis(100, 75, 0.9)
        this.drawMainAxis()

        if (AppContext.currentProject === null) return

        if (AppContext.simOn) {
            this.particlesList = AppContext.currentProject.frames[AppContext.simFrame]['particles']
            if (!AppContext.simPause) {
                if (this.selectedElement !== -1) this.CenterOnParticleId(this.selectedElement)
                AppContext.simFrame++
                if (AppContext.simFrame >= AppContext.currentProject.frames.length) AppContext.simFrame = 0
            }
        } else {
            this.particlesList = AppContext.currentProject.particles
        }

        this.shapesList = AppContext.currentProject.shapes

        this.shapesList.forEach(element => element.Draw(this.drawer, AppContext.currentProject.scale))
        this.particlesList.forEach(element => element.Draw(this.drawer))

        if (this.selectedElement > -1) {
            this.particlesList[this.selectedElement].Draw(this.drawer)
            this.particlesList[this.selectedElement].DrawSelected(this.drawer)
        }
    }

    /**
     * Draw the main axis of the grid
     */
    drawMainAxis() {
        this.p.stroke(this.theme.main_axis)
        this.p.strokeWeight(1)
        this.p.line(this.cx, 0, this.cx, document.getElementById(this.parentID).offsetHeight)
        this.p.line(0, this.cy, document.getElementById(this.parentID).offsetWidth, this.cy)
    }

    drawMultipleAxis(space, stroke, weight) {
        let xOffset = this.cx > 0 ? this.cx % space : space - (Math.abs(this.cx) % space)
        let yOffset = this.cy > 0 ? this.cy % space : space - (Math.abs(this.cy) % space)

        this.p.stroke(stroke)
        this.p.strokeWeight(weight)
        while (xOffset < this.p.width || yOffset < this.p.height) {
            this.p.line(xOffset, 0, xOffset, this.p.height)
            this.p.line(0, yOffset, this.p.width, yOffset)

            xOffset += space
            yOffset += space
        }
    }

    /**
     * Set absolute center of the grid
     * @param x {Number}
     * @param y {Number}
     */
    setAbsCenter(x, y) {
        this.cx = x
        this.cy = y
        this.rx = this.cx / document.getElementById(this.parentID).offsetWidth
        this.ry = this.cy / document.getElementById(this.parentID).offsetHeight
    }

    /**
     * Set relative center of the grid
     * @param rx {Number}
     * @param ry {Number}
     */
    setRelCenter(rx, ry) {
        this.rx = rx
        this.ry = ry
        this.cx = this.rx * document.getElementById(this.parentID).offsetWidth
        this.cy = this.ry * document.getElementById(this.parentID).offsetHeight
    }

    /**
     * Called when user drags the mouse over the grid
     */
    onMouseDragged() {
        if (AppContext.mode < 1) return
        if (this.mouseIsOnGrid()) {
            if (this.p.mouseButton === this.p.RIGHT) {
                this.p.cursor('move')
                this.setAbsCenter(this.cx + this.p.movedX, this.cy + this.p.movedY)
            } else if (this.p.mouseButton === this.p.LEFT && AppContext.mode >= 2) {
                if (this.selectedElement > -1) {
                    this.particlesList[this.selectedElement].Move(this.p.movedX, -this.p.movedY, AppContext.currentProject.scale)
                    AppContext.currentProject.resetSim()
                    this.p.cursor('grabbing')
                }
            }
        } else {
            this.selectedElement = -1
        }
    }

    /**
     * Test if cursor is inside any of the elements contained in array
     */
    findSelectedParticle() {
        for (let i = this.particlesList.length - 1; i >= 0; i--) {
            if (this.particlesList[i].IsInside(this.p.pmouseX - this.cx, this.cy - this.p.pmouseY)) {
                return i
            }
        }
        return -1
    }

    onMousePressed() {
        if (this.mouseIsOnGrid() && this.p.mouseButton === this.p.LEFT && AppContext.mode >= 1) {
            this.selectedElement = this.findSelectedParticle()
            if (this.selectedElement !== -1 && AppContext.mode >= 2) this.p.cursor('grab')
        }
    }

    /**
     * Called when window is resized
     */
    onWindowResized() {
        this.setRelCenter(this.rx, this.ry)
        this.p.resizeCanvas(document.getElementById(this.parentID).offsetWidth, document.getElementById(this.parentID).offsetHeight)
    }

    /**
     * Called when mouseclick is released
     */
    onMouseReleased() {
        this.p.cursor('default')
    }

    /**
     * Return true if cursor is on the grid
     * @return {boolean} mouse in on the grid
     */
    mouseIsOnGrid() {
        return this.p.mouseY >= 0
            && this.p.mouseY < document.getElementById(this.parentID).offsetHeight
            && this.p.mouseX >= 0
            && this.p.mouseX < document.getElementById(this.parentID).offsetWidth
    }
}

export default Grid
