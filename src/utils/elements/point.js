export default class Point {
    /**
     * @param x {Number} x coordinate of the point
     * @param y {Number} y coordinate of the point
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
