package app

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/toqueteos/webbrowser"

	"gitlab.com/psns/sie/app/structs"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(req *http.Request) bool { return !structs.Args.NoCors },
}

// Create the main pool (Only one pool can be used in the app version).
var pool = structs.NewPool(0, "MainPool")

// Start is websocket server entry point.
func Start() {
	fmt.Println("Starting WebSocket server on port : " + structs.Args.Port)

	// Open default Web browser at http://localhost:PORT/
	err := webbrowser.Open(fmt.Sprintf("http://localhost:%s/", structs.Args.Port))
	if err != nil {
		log.Fatalln(err)
	}

	// Create a goroutine for each new client connection.
	http.HandleFunc("/ws", func(res http.ResponseWriter, req *http.Request) {
		// Upgrade HTTP API to websocket
		if conn, err := upgrader.Upgrade(res, req, nil); err != nil {
			log.Fatalln(err)
		} else {
			client := structs.NewClient(conn)
			go client.Listen(pool)

			pool.AddClient(client)
		}
	})

	http.Handle("/", http.FileServer(http.Dir("./dist")))

	// Serve websocket on configured port
	if err := http.ListenAndServe(":"+structs.Args.Port, nil); err != nil {
		panic(err)
	}
}
