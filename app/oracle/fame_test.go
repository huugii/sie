package oracle

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/psns/sie/app/physx"
)

func TestNewFrame(t *testing.T) {
	assert.Equal(t, 0, len(NewFrame().Particles))
}

func TestFrame_GenerateNextFrame(t *testing.T) {
	s := make([]physx.Shape, 0)
	p1 := physx.NewParticle(-1e-9, 0, physx.QPROTON, physx.MPROTON)
	p2 := physx.NewParticle(1e-9, 0, physx.QPROTON, physx.MPROTON)
	f := NewFrame()
	f.Particles = append(f.Particles, p1, p2)
	assert.Equal(t, 2, len(f.Particles))

	f = *f.GenerateNextFrame(s, 2e-20)
	assert.Equal(t, 2, len(f.Particles))
}
