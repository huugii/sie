package structs

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"math"
	"os"
	"path"
	"regexp"

	"gitlab.com/psns/sie/app/oracle"
	"gitlab.com/psns/sie/app/physx"
	"gitlab.com/psns/sie/app/utils"
)

// FileType define a file type.
type FileType int

// Project export file format.
const (
	FileFormatCSV     FileType = iota // Export as CSV
	FileFormatJSON                    // Export as JSON file
	FileFormatHTML                    // Export as static HTML file
	FileFormatMathLab                 // Export as MathLab array
	FileFormatText                    // Export as Text/doc file
)

// Project struct for p formatting.
type Project struct {
	Name       string            `json:"name"`       // Project name (*.siex)
	Time       float64           `json:"time"`       // Simulation time
	TimeScale  float64           `json:"time_scale"` // Time between 2 frames
	Scale      float64           `json:"scale"`      // Distance scale in m/pixel
	Particles  []*physx.Particle `json:"particles"`  // Array of particle
	JSONShapes []physx.JSONShape `json:"shapes"`     // Array of shape
	Frames     []oracle.Frame    `json:"frames"`     // Array of frame
}

// NewProject create new project error.
func NewProject(pName string, pTime, pTimeScale float64) (*Project, error) {
	// Format name
	if !regexp.MustCompile("^[a-zA-Z0-9-_]*." + utils.DefaultFileExtension + "$").Match([]byte(pName)) {
		return nil, errors.New("bad file name format")
	}

	return &Project{
		Name:       pName,
		Time:       pTime,
		TimeScale:  pTimeScale,
		Particles:  make([]*physx.Particle, 0),
		JSONShapes: make([]physx.JSONShape, 0),
		Frames:     make([]oracle.Frame, 0),
	}, nil
}

// Render render all frames of project.
func (p *Project) Render() {
	frame := oracle.NewFrame()
	frame.Particles = p.Particles

	shapes := make([]physx.Shape, 0)
	for _, shape := range p.JSONShapes {
		shapes = append(shapes, physx.Convert(shape))
	}

	frameNumber := int(math.Ceil(p.Time / p.TimeScale))
	for frameIdx := 0; frameIdx < frameNumber; frameIdx++ {
		// Calculate frames
		frame = *frame.GenerateNextFrame(shapes, p.TimeScale)
		p.Frames = append(p.Frames, frame)
	}
}

// Save a file on the computer.
func (p *Project) Save() *utils.SError {
	wd, err := os.Getwd()
	if err != nil {
		return utils.NewSError(utils.SaveFileKo, err)
	}

	saveDir := path.Join(wd, utils.DefaultFileDir)
	if _, errStat := os.Stat(saveDir); errStat != nil {
		if errMkdir := os.Mkdir(saveDir, 0770); errMkdir != nil {
			return utils.NewSError(utils.SaveFileCreateDirKo, errMkdir)
		}
	}

	if !regexp.MustCompile("^[a-zA-Z0-9-_]*." + utils.DefaultFileExtension + "$").Match([]byte(p.Name)) {
		return utils.NewSError(utils.SaveFileKo, errors.New("bad file name format"))
	}

	// Save
	saveData, errEncode := json.Marshal(p) // Marshal p
	if errEncode != nil {
		return utils.NewSError(utils.SaveFileInvalidData, errEncode)
	}

	// Format file path
	filePath := path.Join(saveDir, p.Name)

	errWrite := ioutil.WriteFile(filePath, saveData, 0600) // Write p in file
	if errWrite != nil {
		return utils.NewSError(utils.SaveFileKo, errWrite)
	}

	return nil
}

// ExportAs is a function to export project p as specified format.
func (p *Project) ExportAs(format FileType) *utils.SError {
	if format == FileFormatCSV {
		err := utils.ExportProjectAsCsv()
		if err != nil {
			return err
		}
	}
	return nil
}

// Load a saved file.
func (p *Project) Load(saveFileName string) *utils.SError {
	// Format path from default saves dir.
	saveFilePath := path.Join(utils.DefaultFileDir, saveFileName)

	// error if file not exist
	_, fileErr := os.Stat(saveFilePath)
	if fileErr != nil {
		return utils.NewSError(utils.LoadFileNotFound, fileErr)
	}

	// Read saved p json
	readData, readErr := ioutil.ReadFile(saveFilePath)
	if readErr != nil {
		return utils.NewSError(utils.LoadFileReadError, readErr)
	}

	// Decode p
	decErr := json.Unmarshal(readData, &p)
	if decErr != nil {
		return utils.NewSError(utils.LoadFileDecodeError, decErr)
	}

	return nil
}

// Remove a saved file.
func (p *Project) Remove() *utils.SError {
	// Format paths from default saves dir.
	savedFilePath := path.Join(utils.DefaultFileDir, p.Name)

	info, err := os.Stat(savedFilePath)
	if err != nil {
		return utils.NewSError(utils.RemoveProjectKo, err)
	} else if info.IsDir() {
		return utils.NewSError(utils.RemoveProjectKo, errors.New("you cannot remove directory"))
	} else if err := os.Remove(savedFilePath); err != nil {
		return utils.NewSError(utils.RemoveProjectKo, err)
	}
	return nil
}

// Edit a saved file.
func (p *Project) Edit(newSaveFileName string) *utils.SError {
	// Format paths from default saves dir.
	oldSaveFileName := p.Name

	p.Name = newSaveFileName
	if errSave := p.Save(); errSave != nil {
		// Remove new file if save() create it but gone in error
		if _, err := os.Stat(oldSaveFileName); err == nil {
			_ = os.Remove(oldSaveFileName)
		}
		return utils.NewSError(utils.SaveFileKo, errSave)
	}

	p.Name = oldSaveFileName
	if errRm := p.Remove(); errRm != nil {
		return utils.NewSError(utils.SaveFileKo, errRm)
	}

	return nil
}
