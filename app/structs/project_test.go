package structs

import (
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/psns/sie/app/physx"
	"gitlab.com/psns/sie/app/utils"
)

func TestInit(t *testing.T) {
	workingDir, _ := os.Getwd()
	err := os.MkdirAll(path.Join(workingDir, utils.DefaultFileDir), 0770)
	assert.Nil(t, err)
}

func TestNewProject(t *testing.T) {
	prj, err := NewProject("test.siex", 1, 0.05)
	assert.NotNil(t, prj)
	assert.NoError(t, err)
	assert.Equal(t, 1.00, prj.Time)
	assert.Equal(t, 0.05, prj.TimeScale)
	assert.Equal(t, "test.siex", prj.Name)

	prj, err = NewProject("A sdqs é.siex", 1, 0.05)
	assert.Nil(t, prj)
	assert.Error(t, err)
}

func TestProject_Save(t *testing.T) {
	// Create well named project
	prj, err := NewProject("test.siex", 1, 0.05)
	assert.NoError(t, err)
	assert.NotNil(t, prj)

	err = prj.Save()
	assert.Nil(t, err)

	dir, _ := os.Getwd()
	file := path.Join(dir, utils.DefaultFileDir, prj.Name)

	info, statErr := os.Stat(file)
	assert.NoError(t, statErr)
	assert.NotNil(t, info)

	assert.Equal(t, prj.Name, info.Name())
}

func TestProject_Edit(t *testing.T) {
	// Create well named project
	prj, err := NewProject("test.siex", 1, 0.05)
	assert.NoError(t, err)
	assert.NotNil(t, prj)

	err = prj.Save()
	assert.Nil(t, err)

	err = prj.Edit("test_edit.siex")
	assert.Nil(t, err)
}

func TestProject_Remove(t *testing.T) {
	// Create well named project
	prj, err := NewProject("test.siex", 1, 0.05)
	assert.NoError(t, err)
	assert.NotNil(t, prj)
	err = prj.Save()
	assert.Nil(t, err)

	err = prj.Remove()
	assert.Nil(t, err)
}

func TestProject_ExportAs(t *testing.T) {
	particle0 := physx.NewParticle(0, 0, physx.QELECTRON, physx.MELECTRON)
	particle1 := physx.NewParticle(-10, 10, physx.QPROTON, physx.MPROTON)
	prjParticles := []*physx.Particle{particle0, particle1}

	circle := physx.JSONShape{
		Type: "CIRCLE",
		Info: map[string]float64{
			"center_x": 0,
			"center_y": 0,
			"radius":   10,
		},
	}
	rectangle := physx.JSONShape{
		Type: "RECTANGLE",
		Info: map[string]float64{
			"p1_x": 0,
			"p1_y": 0,
			"p2_x": -10,
			"p2_y": 10,
		},
	}
	prjShape := []physx.JSONShape{circle, rectangle}

	prj, err := NewProject("test.siex", 1, 0.05)
	assert.NoError(t, err)
	assert.NotNil(t, prj)

	prj.Particles = prjParticles
	prj.JSONShapes = prjShape

	assert.Nil(t, prj.ExportAs(FileFormatCSV))
}

func TestProject_LoadOk(t *testing.T) {
	particle0 := physx.NewParticle(-10, 0, physx.QELECTRON, physx.MELECTRON)
	particle1 := physx.NewParticle(10, 0, physx.QPROTON, physx.MPROTON)
	prjParticles := []*physx.Particle{particle0, particle1}

	circle := physx.JSONShape{
		Type: "CIRCLE",
		Info: map[string]float64{
			"center_x": 0,
			"center_y": 0,
			"radius":   10,
		},
	}
	rectangle := physx.JSONShape{
		Type: "RECTANGLE",
		Info: map[string]float64{
			"p1_x": 0,
			"p1_y": 0,
			"p2_x": -10,
			"p2_y": 10,
		},
	}
	prjShape := []physx.JSONShape{circle, rectangle}

	prj, err := NewProject("load.siex", 1, 0.05)
	assert.NoError(t, err)
	assert.NotNil(t, prj)

	prj.Particles = prjParticles
	prj.JSONShapes = prjShape

	err = prj.Save()
	assert.Nil(t, err)
}

func TestProject_LoadError(t *testing.T) {
	// Init expected file dir from project wd
	file := "load.siex"

	// Test load file not found
	dataErrNotfound := Project{}
	errLoad := dataErrNotfound.Load("abcdef.siex")
	assert.NotNil(t, errLoad)
	assert.Equal(t, utils.LoadFileNotFound, errLoad.Code)
	assert.Equal(t, Project{}, dataErrNotfound)

	// Test load file read error
	gitEnv := os.Getenv("GITLAB_CI")
	if gitEnv != "true" {
		dataErrRead := Project{}
		errChmod := os.Chmod(path.Join(utils.DefaultFileDir, file), 0110)
		assert.Nil(t, errChmod)

		errLoad = dataErrRead.Load(file)
		assert.NotNil(t, errLoad)
		assert.Equal(t, utils.LoadFileReadError, errLoad.Code)
		assert.Equal(t, Project{}, dataErrRead)

		errChmod = os.Chmod(path.Join(utils.DefaultFileDir, file), 0770)
		assert.Nil(t, errChmod)
	}

	// Test load file decode error
	// bad data to simulate decode data
	dataErrDecode := Project{}
	file = "CustomLoad2.siex"
	dataByte := []byte(`{
		"time": 1,
		"time_ddqzscale": 0.05,
		"particles": [],
		"shapezzs": [
			{
				"d": "RECTANGLE",
					"p1_x": "0",
					"p1_y": "0",
					"p2_x": "-10",
					"p2_y": "10"
				}
			}
		]
	}`)
	errWrite := ioutil.WriteFile(path.Join(utils.DefaultFileDir, file), dataByte, 0600)
	assert.Nil(t, errWrite)

	errLoad = dataErrDecode.Load(file)
	assert.NotNil(t, errLoad)
	assert.Equal(t, utils.LoadFileDecodeError, errLoad.Code)
	assert.Equal(t, Project{}, dataErrDecode)
}

func TestList(t *testing.T) {
	saveFiles, err := utils.ListProjects()
	assert.Nil(t, err)
	assert.NotNil(t, saveFiles)
	reg := regexp.MustCompile(".siex$")
	for _, file := range saveFiles {
		res := reg.Match([]byte(file.Name()))
		assert.True(t, res)
	}
}

func TestClean(t *testing.T) {
	workingDir, _ := os.Getwd()
	err := os.RemoveAll(path.Join(workingDir, utils.DefaultFileDir))
	assert.Nil(t, err)
}
