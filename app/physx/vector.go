package physx

import (
	"math"
)

// Vector define a 2D vector in space.
type Vector struct {
	Vx float64 `json:"vx"` // vector X coordinate in meters
	Vy float64 `json:"vy"` // vector Y coordinate in meters
}

// DirectionAngle return the direction angle of the vector.
func (v Vector) DirectionAngle() float64 {
	return math.Atan2(v.Vy, v.Vx)
}

// NewVectorByPoint create a new vector using two points.
func NewVectorByPoint(a, b Point) Vector {
	return Vector{Vx: b.X - a.X, Vy: b.Y - a.Y}
}

// NewVectorByCoordinate create a new vector using coordinates and an application point.
func NewVectorByCoordinate(x, y float64) Vector {
	return Vector{Vx: x, Vy: y}
}

// ScalarProduct return the scalar product of 2 vectors.
func ScalarProduct(vec1, vec2 *Vector) float64 {
	return (vec2.Vx - vec1.Vx) + (vec2.Vy - vec1.Vy)
}

// MultiplyVector multiply a vector by a float value.
func MultiplyVector(vec *Vector, value float64) Vector {
	return Vector{Vx: vec.Vx * value, Vy: vec.Vy * value}
}
