package physx

import (
	"math"
)

// CONSTK is the constant of Coulomb Law.
const CONSTK = 8.987551787368176e+9

// CoulombLaw calculate the coulomb law between two charges.
func CoulombLaw(c1, c2 *Particle) float64 {
	return CONSTK * (c1.Q * c2.Q) / math.Pow(GetDistance(c1.Pos, c2.Pos), 2)
}

// CoulombForce calculate the force vector.
func CoulombForce(c1, c2 *Particle) Vector {
	v12 := NewVectorByPoint(c1.Pos, c2.Pos) // Vector between two charges
	a := v12.DirectionAngle()               // Angle the vector
	resCoulombLaw := CoulombLaw(c1, c2)     // Coulomb Law of charges

	return NewVectorByCoordinate(-math.Cos(a)*resCoulombLaw, -math.Sin(a)*resCoulombLaw)
}
