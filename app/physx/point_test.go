package physx

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewPoint(t *testing.T) {
	x, y := float64(3), float64(4)
	p := NewPoint(x, y)

	assert.Equal(t, float64(3), p.X)
	assert.Equal(t, float64(4), p.Y)
}

func TestPoint_GetDistance(t *testing.T) {
	x1, y1 := float64(-1), float64(-1)
	x2, y2 := float64(1), float64(-1)
	p1 := NewPoint(x1, y1)
	p2 := NewPoint(x2, y2)

	d := GetDistance(p1, p2)

	assert.Equal(t, d, float64(2))
}
